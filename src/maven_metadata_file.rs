#[derive(serde::Deserialize, Debug)]
pub struct MavenMetadata {
    pub versioning: Versioning
}

#[derive(serde::Deserialize, Debug)]
pub struct Versioning {
    pub release: String,
}