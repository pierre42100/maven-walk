use core::fmt;
use std::error;
use std::error::Error;
use std::fmt::{Formatter, Display};

/// Simple rust error
///
/// @author Pierre Hubert


/// Simple result type
pub type ResultExecError<E> = Result<E, ExecError>;
pub type ResultBoxError<E = ()> = Result<E, Box<dyn Error>>;
pub type Res<E = ()> = ResultBoxError<E>;

#[derive(Debug, Clone)]
pub struct ExecError(pub String);

impl ExecError {
    pub fn new<S: Display>(msg: S) -> ExecError {
        ExecError(msg.to_string())
    }

    pub fn boxed_new(msg: &str) -> Box<ExecError> {
        Box::new(ExecError(msg.to_string()))
    }

    pub fn boxed_string(msg: String) -> Box<ExecError> {
        Box::new(ExecError(msg))
    }
}

impl fmt::Display for ExecError {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        write!(f, "Encountered error: {}", self.0)
    }
}


impl error::Error for ExecError {}
