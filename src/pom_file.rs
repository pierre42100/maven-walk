use std::collections::HashMap;

pub type ManifestProperties = Option<HashMap<String, String>>;

#[derive(serde::Deserialize, Debug)]
pub struct Project {
    pub dependencies: Option<Dependencies>,
    pub parent: Option<Dependency>,
    #[serde(rename = "dependencyManagement")]
    pub dependency_management: Option<DependenyManagement>,
    pub properties: ManifestProperties,
}

#[derive(serde::Deserialize, Debug)]
pub struct DependenyManagement {
    pub dependencies: Option<Dependencies>,
}

#[derive(serde::Deserialize, Debug)]
pub struct Dependencies {
    pub dependency: Option<Vec<Dependency>>
}

#[derive(serde::Deserialize, Debug, Clone)]
pub struct Dependency {
    #[serde(rename = "groupId")]
    pub group_id: String,
    #[serde(rename = "artifactId")]
    pub artifact_id: String,
    #[serde(rename = "version")]
    pub version: Option<String>,
    pub scope: Option<String>,
}

impl Dependency {
    pub fn short_summary(&self) -> String {
        format!(
            "{}:{}:{}",
            self.group_id,
            self.artifact_id,
            self.version.as_ref().map(String::as_str).unwrap_or("")
        )
    }

    pub fn file_url(&self, repository: &str, ext: &str) -> String {
        let mut version = self.version.as_ref().unwrap().as_str();
        if version.starts_with("[") && version.ends_with("]") {
            version = &version[1..version.len() - 1];
        }

        format!("{}/{}/{}/{}/{}-{}{}",
                repository,
                self.group_id.replace(".", "/"),
                self.artifact_id,
                version,
                self.artifact_id,
                version,
                ext
        )
    }

    fn apply_properties(s: &str, map: &HashMap<String, String>, parent: &Dependency) -> String {
        let mut s = s.to_string();
        for (k, v) in map {
            s = s.replace(&format!("${{{}}}", k), v);
        }

        s
            .replace("${project.groupId}", &parent.group_id)
            .replace("${project.version}", &parent.version.as_ref().map(String::as_str)
                .unwrap_or(""))
    }

    pub fn update_with_properties(&mut self, map: &HashMap<String, String>, parent: &Dependency) {
        self.group_id = Self::apply_properties(&self.group_id, map, parent);
        self.artifact_id = Self::apply_properties(&self.artifact_id, map, parent);
        if let Some(vers) = &self.version {
            self.version = Some(Self::apply_properties(vers, map, parent));
        }
    }
}

impl PartialEq for Dependency {
    fn eq(&self, other: &Self) -> bool {
        self.group_id == other.group_id &&
            self.artifact_id == other.artifact_id &&
            self.version == other.version
    }
}
