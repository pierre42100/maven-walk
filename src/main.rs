use std::collections::HashMap;
use std::error::Error;
use std::process;

use crate::config::Config;
use crate::error::{ExecError, Res};
use crate::maven_metadata_file::MavenMetadata;
use crate::pom_file::{Dependencies, Dependency, ManifestProperties, Project};

mod pom_file;
mod maven_metadata_file;
mod config;
pub mod error;

const INTERESTING_EXTS: [&str; 42] = [
    "-javadoc.jar",
    "-javadoc.jar.asc",
    "-javadoc.jar.asc.md5",
    "-javadoc.jar.asc.sha1",
    "-javadoc.jar.asc.sha256",
    "-javadoc.jar.asc.sha512",
    "-javadoc.jar.md5",
    "-javadoc.jar.sha1",
    "-javadoc.jar.sha256",
    "-javadoc.jar.sha512",
    "-sources.jar",
    "-sources.jar.asc",
    "-sources.jar.asc.md5",
    "-sources.jar.asc.sha1",
    "-sources.jar.asc.sha256",
    "-sources.jar.asc.sha512",
    "-sources.jar.md5",
    "-sources.jar.sha1",
    "-sources.jar.sha256",
    "-sources.jar.sha512",
    ".jar",
    ".aar",
    ".module",
    ".jar.asc",
    ".jar.asc.md5",
    ".jar.asc.sha1",
    ".jar.asc.sha256",
    ".jar.asc.sha512",
    ".jar.md5",
    ".jar.sha1",
    ".jar.sha256",
    ".jar.sha512",
    ".pom",
    ".pom.asc",
    ".pom.asc.md5",
    ".pom.asc.sha1",
    ".pom.asc.sha256",
    ".pom.asc.sha512",
    ".pom.md5",
    ".pom.sha1",
    ".pom.sha256",
    ".pom.sha512",
];

fn main() {
    env_logger::init();

    let args = std::env::args().collect::<Vec<_>>();

    if args.len() < 5 {
        eprintln!("Usage: {} [group] [artifact] [version] [repository...]", args[0]);
        process::exit(2);
    }

    let conf = Config {
        group: args[1].to_string(),
        artifact: args[2].to_string(),
        version: args[3].to_string(),
        repositories: args[4..].to_vec(),
        max_recursion: Some(20),
    };

    log::debug!("{:#?}", conf);

    recurse_scan(&conf, &mut conf.initial_dependency(), &vec![], &mut vec![]).unwrap();
}

fn download_pom(repository: &str, d: &Dependency) -> Option<Project> {
    let url = d.file_url(repository, ".pom");
    let res: Result<Project, ExecError> = reqwest::blocking::get(url).map(|r| r.text())
        .and_then(|r| r)
        .map_err(ExecError::new)
        .map(|s| serde_xml_rs::from_str(&s))
        .and_then(|r| r.map_err(ExecError::new));

    match res {
        Ok(p) => Some(p),
        Err(e) => {
            log::warn!("Failed to get manifest for {} on {}! {}", d.short_summary(), repository, e);
            None
        }
    }
}

fn download_manifest(repository: &str, d: &Dependency) -> Option<MavenMetadata> {
    let url = format!(
        "{}/{}/{}/maven-metadata.xml",
        repository,
        d.group_id.replace(".", "/"),
        d.artifact_id
    );

    let res: Result<MavenMetadata, ExecError> = reqwest::blocking::get(url).map(|r| r.text())
        .and_then(|r| r)
        .map_err(ExecError::new)
        .map(|s| serde_xml_rs::from_str(&s))
        .and_then(|r| r.map_err(ExecError::new));

    match res {
        Ok(p) => Some(p),
        Err(e) => {
            log::warn!("Failed to get metadata for {} on {}! {}", d.short_summary(), repository, e);
            None
        }
    }
}

fn recurse_scan(c: &Config, d: &mut Dependency, stack: &[Dependency], already_visited: &mut Vec<Dependency>) -> Result<(), Box<dyn Error>> {
    log::info!("Process {}", d.short_summary());

    // Check if we reached recursion limit
    if let Some(max_recur) = c.max_recursion {
        if max_recur < stack.len() {
            log::warn!("Ignore {} due to recursion limit!", d.short_summary());
            return Ok(());
        }
    }

    let mut print = String::new();
    for _ in 0..stack.len() {
        print.push(' ');
    }
    eprintln!("{}+{}", print, d.short_summary());

    if already_visited.contains(d) {
        return Ok(());
    }

    already_visited.push(d.clone());

    let mut stack = stack.to_vec();
    stack.push(d.clone());

    let stack_str = stack.iter().map(|s| s.short_summary()).collect::<Vec<_>>().join(" > ");

    if let Some(scope) = &d.scope {
        if /*scope != "compile" && scope != "runtime"*/ scope == "test" {
            log::warn!("Skip {} because of its scope {} !", stack_str, scope);
            return Ok(());
        }
    }

    if d.short_summary().contains("$") {
        log::warn!("Skip {} because it contains some un-expanded variables !", stack_str);
        return Ok(());
    }


    // First, check if we need to determine dependency version
    if d.version.is_none() {
        log::warn!("Has to determine manually package version...");
        d.version = c.repositories.iter()
            .map(|r| download_manifest(r, d))
            .find(|r| r.is_some())
            .unwrap_or(None)
            .map(|f| f.versioning.release);

        d.version.as_ref()
            .ok_or(ExecError::new(format!("Failed to determine latest package version of {}!", d.short_summary())))?;
    }

    let manifest = c.repositories
        .iter()
        .map(|r| (r, download_pom(r, d)))
        .find(|f| f.1.is_some())
        .ok_or(ExecError::new(format!(
            "Failed to fetch a manifest for the dependency! {}",
            stack_str))
        )?;

    let (repository, manifest) = (manifest.0, manifest.1.unwrap());
    log::info!("Found {}  on {}", d.short_summary(), repository);

    // Print relevant URLS
    for ext in INTERESTING_EXTS {
        println!("{}", d.file_url(repository, ext));
    }

    // Scan dependencies
    if let Some(dep) = manifest.parent {
        let deps = Dependencies { dependency: Some(vec![dep]) };
        scan_deps(c, d, &stack, &manifest.properties, deps, already_visited)?;
    }
    if let Some(deps) = manifest.dependencies {
        scan_deps(c, d, &stack, &manifest.properties, deps, already_visited)?;
    }
    if let Some(mgmt) = manifest.dependency_management {
        if let Some(deps) = mgmt.dependencies {
            scan_deps(c, d, &stack, &manifest.properties, deps, already_visited)?;
        }
    }

    Ok(())
}

fn scan_deps(c: &Config, parent: &Dependency, stack: &[Dependency],
             manifest_properties: &ManifestProperties,
             deps: Dependencies,
             already_visited: &mut Vec<Dependency>) -> Res {
    if let Some(mut deps) = deps.dependency {
        for child_dep in &mut deps {
            if let Some(props) = &manifest_properties {
                child_dep.update_with_properties(props, parent);
            } else {
                child_dep.update_with_properties(&HashMap::new(), parent);
            }

            if let Err(e) = recurse_scan(c, child_dep, &stack, already_visited) {
                eprintln!("Failed to fetch {} ! {}", child_dep.short_summary(), e);
            }
        }
    }

    Ok(())
}