use crate::pom_file::Dependency;

#[derive(Debug)]
pub struct Config {
    pub group: String,
    pub artifact: String,
    pub version: String,
    pub repositories: Vec<String>,
    pub max_recursion: Option<usize>,
}

impl Config {
    pub fn initial_dependency(&self) -> Dependency {
        Dependency {
            group_id: self.group.to_string(),
            artifact_id: self.artifact.to_string(),
            version: Some(self.version.to_string()),
            scope: None,
        }
    }
}